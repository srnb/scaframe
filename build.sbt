lazy val scaframe = (project in file("scaframe")).settings(
  organization := "tf.bug",
  name := "scaframe",
  version := "0.1.0",
  scalaVersion := "2.12.8",
  resolvers ++= Seq(
    Resolver.bintrayRepo("oyvindberg", "ScalablyTyped"),
  ),
  libraryDependencies ++= Seq(
    "org.typelevel" %%% "cats-core" % "2.0.0-M1",
    "org.typelevel" %%% "cats-effect" % "2.0.0-M2",
  ),
  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-feature",
    "-Ypartial-unification",
    "-language:higherKinds",
  ),
  mainClass := Some("tf.bug.scaframe.Main"),
  scalaJSUseMainModuleInitializer := true,
).dependsOn(facaframe).enablePlugins(ScalaJSPlugin)

lazy val facaframe = (project in file("facaframe")).settings(
  organization := "tf.bug",
  name := "facaframe",
  version := "0.1.0",
  scalaVersion := "2.12.8",
  resolvers ++= Seq(
    Resolver.bintrayRepo("oyvindberg", "ScalablyTyped"),
  ),
  libraryDependencies ++= Seq(
    "org.scala-js" %%% "scalajs-dom" % "0.9.7",
    ScalablyTyped.A.aframe,
  ),
  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-feature",
    "-Ypartial-unification",
    "-language:higherKinds",
  ),
).enablePlugins(ScalaJSPlugin)
