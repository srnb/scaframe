package tf.bug.facaframe

import scalajs.js
import typings.aframeLib.{aframeMod => aframe}

trait Node extends GenNode[aframe.ANode] {

  val unsafe: aframe.ANode

  def setAttribute(attr: String, value: js.Any): Unit = unsafe.setAttribute(attr, value)

}
