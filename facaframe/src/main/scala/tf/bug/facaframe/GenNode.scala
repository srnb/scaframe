package tf.bug.facaframe

import typings.aframeLib.{aframeMod => aframe}

trait GenNode[+U <: aframe.ANode] {

  val unsafe: U

}
