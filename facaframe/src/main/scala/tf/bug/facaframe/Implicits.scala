package tf.bug.facaframe

import typings.aframeLib.{aframeMod => aframe}

object implicits {

  implicit def entityUnsafe: Unsafe.Aux[Entity, aframe.Entity[_]] = new Unsafe[Entity] {

    override type T = aframe.Entity[_]

    def to(a: Entity): aframe.Entity[_] = a.unsafe
    def from(u: aframe.Entity[_]): Entity = new Entity { override val unsafe = u }

  }

  implicit def nodeUnsafe: Unsafe.Aux[Node, aframe.ANode] = new Unsafe[Node] {

    override type T = aframe.ANode

    def to(a: Node): aframe.ANode = a.unsafe
    def from(u: aframe.ANode): Node = new Node { override val unsafe = u }

  }

}
