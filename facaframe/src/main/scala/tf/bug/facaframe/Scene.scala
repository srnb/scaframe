package tf.bug.facaframe

import org.scalajs.dom._
import typings.aframeLib.{aframeMod => aframe}

object Scene {

  def apply(): Scene = {
    val html = document.querySelector("a-scene").asInstanceOf[aframe.Scene]
    Scene(html)
  }

}

case class Scene private (unsafe: aframe.Scene) {

  def +=[A, U <: aframe.ANode](node: A)(implicit ev: Unsafe.Aux[A, U]): A = {
    val html = ev.to(node)
    val newHtml = unsafe.appendChild(html)
    console.log("Appended ", newHtml)
    ev.from(newHtml)
  }

  def -=[A, U <: aframe.ANode](node: A)(implicit ev: Unsafe.Aux[A, U]): Unit = {
    val html = ev.to(node)
    unsafe.removeChild(html)
  }

}
