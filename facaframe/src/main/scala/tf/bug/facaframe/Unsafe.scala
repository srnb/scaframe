package tf.bug.facaframe

trait Unsafe[A] {

  type T

  def from(u: T): A
  def to(a: A): T

}

object Unsafe {

  type Aux[A, U] = Unsafe[A] { type T = U }

}
