package tf.bug.facaframe

import org.scalajs.dom._
import typings.aframeLib.{aframeMod => aframe}

object AFrame {

  def node(name: String)(implicit ev: Unsafe.Aux[Node, aframe.ANode]): Node = {
    val html = document.createElement(s"a-$name").asInstanceOf[aframe.ANode]
    ev.from(html)
  }

  def entity(implicit ev: Unsafe.Aux[Entity, aframe.Entity[_]]): Entity = {
    val html = document.createElement("a-entity").asInstanceOf[aframe.Entity[_]]
    ev.from(html)
  }

}
