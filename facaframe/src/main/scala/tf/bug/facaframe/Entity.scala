package tf.bug.facaframe

import scalajs.js
import typings.aframeLib.{aframeMod => aframe}

trait Entity extends Node with GenNode[aframe.Entity[_]] {

  val unsafe: aframe.Entity[_]

  def setAttribute(attr: String, prop: String, value: js.Any): Unit = unsafe.setAttribute(attr, prop, value)

}
