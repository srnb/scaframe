package tf.bug.scaframe

import cats._
import cats.implicits._
import cats.effect.Sync
import org.scalajs.dom._
import tf.bug.facaframe
import tf.bug.facaframe.implicits._
import tf.bug.scaframe.entity.Entity
import tf.bug.scaframe.node.Node

object AFrame {

  def node[F[_]: Sync](name: String): F[Node[F]] = Sync[F].delay(new Node[F] { override val unsafe = facaframe.AFrame.node(name) })
  def entity[F[_]: Sync]: F[Entity[F]] = Sync[F].delay(Entity[F](facaframe.AFrame.entity))

}
