package tf.bug.scaframe.entity

import tf.bug.facaframe, facaframe.Unsafe

trait Implicits {

  implicit def boxUnsafe[F[_]]: Unsafe.Aux[Box[F], facaframe.Entity] = new Unsafe[Box[F]] {

    override type T = facaframe.Entity

    def from(e: facaframe.Entity): Box[F] = Box[F](Entity[F](e))
    def to(b: Box[F]): facaframe.Entity = b.entity.unsafe

  }

}
