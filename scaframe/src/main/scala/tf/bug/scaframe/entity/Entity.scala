package tf.bug.scaframe.entity

import cats.effect.Sync
import scala.scalajs.js
import tf.bug.facaframe
import tf.bug.scaframe.node.Node

case class Entity[F[_]](unsafe: facaframe.Entity) extends Node[F] {

  def setAttribute(name: String, value: String)(implicit s: Sync[F]): F[Unit] =
    s.delay(unsafe.setAttribute(name, value))

}
