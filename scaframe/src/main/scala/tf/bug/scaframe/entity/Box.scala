package tf.bug.scaframe.entity

import cats._
import cats.implicits._
import cats.effect.Sync
import tf.bug.scaframe.AFrame

case class Box[F[_]] private (entity: Entity[F]) {

  def setPosition(xyz: (Double, Double, Double))(implicit s: Sync[F]): F[Unit] = {
    val (x, y, z) = xyz
    entity.setAttribute("position", Map("x" -> x, "y" -> y, "z" -> z))
  }

  def setSize(whd: (Double, Double, Double))(implicit s: Sync[F]): F[Unit] = {
    val (w, h, d) = whd
    entity.setAttribute(
      "geometry",
      Map(
        "width" -> w,
        "height" -> h,
        "depth" -> d
      )
    )
  }

  def setColor(c: String)(implicit s: Sync[F]): F[Unit] = entity.setAttribute("material", Map("color" -> c))

}

object Box {

  def apply[F[_]: Sync](position: (Double, Double, Double), size: (Double, Double, Double), color: String): F[Box[F]] =
    for {
      ent <- AFrame.entity[F]
      box = Box(ent)
      _ <- box.entity.setAttribute("geometry", Map("primitive" -> "box"))
      _ <- box.setPosition(position)
      _ <- box.setSize(size)
      _ <- box.setColor(color)
    } yield box

}
