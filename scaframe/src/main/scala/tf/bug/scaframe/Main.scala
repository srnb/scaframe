package tf.bug.scaframe

import java.util.concurrent.TimeUnit

import cats._
import cats.implicits._
import cats.effect._
import cats.effect.implicits._
import tf.bug.facaframe.implicits._
import tf.bug.scaframe.entity.Box
import tf.bug.scaframe.node.Sky
import scala.concurrent.duration._

import implicits._

object Main extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = program[IO].as(ExitCode.Success)

  def program[F[_]: Async: Timer]: F[Unit] = {
    for {
      s <- Scene[F]
      b <- (Box[F]((0.0d, 0.0d, -10.0d), (1.0d, 1.0d, 1.0d), "green")
        :: Box[F]((0.0d, 5.0d, -7.0d), (1.0d, 1.0d, 1.0d), "red")
        :: Nil).sequence
      t <- Sky[F]("blue")
      v = s <+= t
      r = b.map(s <+= _)
      q = r.sequence
      u <- q.flatMap(l => v.map(_ :: l)).use(_ => Async[F].never[Unit])
    } yield u
  }
}
