package tf.bug.scaframe

import cats._
import cats.implicits._
import cats.effect.{Resource, Sync}
import tf.bug.facaframe, facaframe.Unsafe, facaframe.implicits._
import tf.bug.scaframe.node.Node
import typings.aframeLib.{aframeMod => aframe}

object Scene {

  def apply[F[_]: Sync]: F[Scene[F]] = Sync[F].delay(Scene(facaframe.Scene()))

}

case class Scene[F[_]](unsafe: facaframe.Scene) {

  def <+=[A, T, U <: aframe.ANode](node: A)(implicit sync: Sync[F], ev: Unsafe.Aux[A, T], ev2: Unsafe.Aux[T, U]): Resource[F, A] = {
    val acquire = sync.delay {
      val elem = ev.to(node)
      val added = unsafe += elem
      ev.from(added)
    }
    val release = (e: A) => sync.delay {
      val elem = ev.to(e)
      unsafe -= elem
    }
    Resource.make(acquire)(release)
  }

}
