package tf.bug.scaframe.node

import cats._
import cats.implicits._
import cats.effect.Sync
import tf.bug.scaframe.AFrame

case class Sky[F[_]] private (node: Node[F]) {

  def setColor(c: String)(implicit s: Sync[F]): F[Unit] = node.setAttribute("material", Map("color" -> c))

}

object Sky {

  def apply[F[_]: Sync](color: String): F[Sky[F]] =
    for {
      ent <- AFrame.node[F]("sky")
      sky = Sky(ent)
      _ <- sky.setColor(color)
    } yield sky

}
