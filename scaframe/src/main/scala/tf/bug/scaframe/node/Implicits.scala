package tf.bug.scaframe.node

import tf.bug.facaframe, facaframe.Unsafe

trait Implicits {

  implicit def skyUnsafe[F[_]]: Unsafe.Aux[Sky[F], facaframe.Node] = new Unsafe[Sky[F]] {

    override type T = facaframe.Node

    def from(n: facaframe.Node): Sky[F] = Sky[F](new Node[F] { override val unsafe = n })
    def to(s: Sky[F]): facaframe.Node = s.node.unsafe

  }

}
