package tf.bug.scaframe.node

import cats.effect.Sync
import scalajs.js
import tf.bug.facaframe

trait Node[F[_]] {

  val unsafe: facaframe.Node

  def setAttribute(name: String, properties: Map[String, _])(implicit s: Sync[F]): F[Unit] = {
    val dict = js.Dictionary(properties.toSeq: _*)
    s.delay(unsafe.setAttribute(name, dict))
  }

}
