addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.28")

resolvers += Resolver.bintrayRepo("oyvindberg", "ScalablyTyped")
addSbtPlugin("org.scalablytyped" % "sbt-scalablytyped" % "201905280530")
